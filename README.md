# README #

El presente proyecto es un API Rest (JAX-RS) para la creacion de arboles y busquedas

### Requisitos ###

* Java 8
* Wildfly 11 o superior
* Cliente (Postman)
* Netbeans

### Despliegue ###

Es necesario construir el proyecto y generar el .war para copiarlo dentro de la carpeta deployments de servidor
de aplicaciones


### Consumo ###

Desde un cliente vamos a realizar el consumo de los 2 metodos de nuestro servicio, el primero

* Crear Arbol

Para realizar esta operación es necesario hacer una peticion POST a este endpoint 
http://localhost:8080/Masivian-1.0/api/resources/crearArbol con el siquiente Request


{  
   "raiz":{  
      "valor":67	
   },
   "arbol":[  
      {  
         "valor":39
      },
      {  
         "valor":28
      },
      {  
         "valor":44
      },
      {  
         "valor":29
      },
      {  
         "valor":76
      },
      {  
         "valor":74
      },
      {  
         "valor":85
      },
      {  
         "valor":83
      },
      {  
         "valor":87
      }
   ]
}

* Buscar Ancestro

Para realizar esta operación es necesario hacer una peticion POST a este endpoint 
http://localhost:8080/Masivian-1.0/api/resources/retornarAncestro la diferencia de este request
con el anterior es que se agrega el valor del nodo A y B los cuales se van a ir a buscar y a determina
que nodo ancestro es el que comparten


{  
   "raiz":{  
      "valor":67
   },
   "arbol":[  
      {  
         "valor":39
      },
      {  
         "valor":28
      },
      {  
         "valor":44
      },
      {  
         "valor":29
      },
      {  
         "valor":76
      },
      {  
         "valor":74
      },
      {  
         "valor":85
      },
      {  
         "valor":83
      },
      {  
         "valor":87
      }
   ],
   "nodoA":{  
      "valor":83
   },
   "nodoB":{  
      "valor":87
   }
}




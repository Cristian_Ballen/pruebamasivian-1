/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.masivian.dto;

import java.util.Objects;

/**
 *
 * @author cballen
 */
//@Stateless
public class Arbol {

    private Nodo raiz;

    public Arbol(Nodo raiz) {
        this.raiz = raiz;
    }

    public Arbol() {

    }

    /* Setters y Getters */
    public Nodo getRaiz() {
        return raiz;
    }

    public void setRaiz(Nodo raiz) {
        this.raiz = raiz;
    }

    private void addNodo(Nodo nodo, Nodo raiz) {

        if (Objects.isNull(raiz)) {
            this.setRaiz(nodo);

        } else {
            if (nodo.getValor() <= raiz.getValor()) {
                if (raiz.getIzq() == null) {
                    nodo.setPadre(raiz);
                    raiz.setIzq(nodo);

                } else {
                    nodo.setPadre(raiz); // ultimo
                    addNodo(nodo, raiz.getIzq());
                }
            } else {
                if (raiz.getDer() == null) {
                    nodo.setPadre(raiz);
                    raiz.setDer(nodo);

                } else {
                    nodo.setPadre(raiz);
                    addNodo(nodo, raiz.getDer());

                }
            }
        }
    }

    public void addNodo(Nodo nodo) {
        this.addNodo(nodo, this.raiz);
    }
}

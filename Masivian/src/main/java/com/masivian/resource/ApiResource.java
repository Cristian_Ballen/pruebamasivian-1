/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.masivian.resource;

import com.masivian.dto.Tree;
import com.masivian.service.IArbol;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author cballen
 */
@Path("/resource")
public class ApiResource {

    @Inject
    private IArbol arbolService;

    @POST
    @Path("crearArbol")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response crearArbol(Tree arbol) {
        try {
            String result = arbolService.crearArbol(arbol);
            return Response.status(200).entity(result).build();
        } catch (Exception e) {
            return Response.status(400).entity("Estructura mal construida "+e.getMessage()).build();
        }
    }

    @POST
    @Path("retornarAncestro")
    public Response retornarAncestro(Tree arbol) {
        try {
            Integer padre = arbolService.retornarAncestro(arbol);
            return Response.status(200).entity(String.valueOf(padre)).build();
        } catch (Exception e) {
            return Response.status(400).entity(e.getMessage()).build();
        }
    }
}

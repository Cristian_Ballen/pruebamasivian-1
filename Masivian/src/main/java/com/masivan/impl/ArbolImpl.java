/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.masivan.impl;

import com.masivian.dto.Arbol;
import com.masivian.dto.Nodo;
import com.masivian.dto.Tree;
import com.masivian.service.IArbol;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import javax.ejb.Stateless;

/**
 * 
 * @author cballen
 */
@Stateless
public class ArbolImpl implements IArbol {

    Nodo raiz;
    StringBuilder result;
    List<Integer> camino;

    @Override
    public String crearArbol(Tree tree) throws Exception{
        Arbol arbol = new Arbol();
        arbol.setRaiz(tree.getRaiz());
        tree.getArbol().forEach((iteNodo) -> {
            arbol.addNodo(iteNodo);
        });
        result = new StringBuilder();
        preOrden(arbol.getRaiz());
        return result.toString();
    }

    /**
     * Ordena un arbol en pre-orden
     * 
     * @param raiz
     */
    private void preOrden(Nodo raiz) {
        if (Objects.nonNull(raiz)) {
            int fina = (raiz.getPadre() == null) ? 0 : raiz.getPadre().getValor();
            result.append("[");
            result.append(raiz.getValor());
            result.append(" ");
            result.append(fina);
            result.append("]");
            result.append("-");
            preOrden(raiz.getIzq());
            preOrden(raiz.getDer());

        }
    }

    @Override
    public Integer retornarAncestro(Tree tree) throws Exception {
        Arbol arbol = new Arbol();
        arbol.setRaiz(tree.getRaiz());
        tree.getArbol().forEach(arbol::addNodo);
        if (Objects.nonNull(arbol.getRaiz()) && tree.getArbol().size() >= 2) {
            camino = new ArrayList<>();
            return buscarAntecesor(arbol, tree.getNodoA(), tree.getNodoB());
        } else {
            throw new Exception("Se requiere raiz y dos hijos");
        }

    }

    /**
     * Retorna el ancestro más cercano
     *
     * @param arbol
     * @param nodoA
     * @param nodoB
     * @return Valor del nodo padre que comparten los nodos
     * @throws java.lang.Exception
     */
    public Integer buscarAntecesor(Arbol arbol, Nodo nodoA, Nodo nodoB) throws Exception {
        List<Integer> caminoA = new ArrayList<>();
        List<Integer> caminoB = new ArrayList<>();
        Arbol copy = new Arbol();
        copy.setRaiz(arbol.getRaiz());
        encontrarNodo(copy, nodoA);
        Collections.reverse(camino);
        caminoA.addAll(camino);
        camino = new ArrayList<>();
        encontrarNodo(arbol, nodoB);
        Collections.reverse(camino);
        caminoB.addAll(camino);

        for (Integer integer : caminoA) {
            if (caminoB.contains(integer)) {
                return integer;
            }
        }
        return 0;
    }

    /**
     * Busca un nodo en una estructura arbol y guarda el camino recorrido
     *
     * @param arbol
     * @param nodo
     */
    public void encontrarNodo(Arbol arbol, Nodo nodo) {
        Nodo aux = arbol.getRaiz();
        camino.add(arbol.getRaiz().getValor());
        if (nodo.getValor() <= aux.getValor()) {
            if (!(Objects.nonNull(aux.getIzq()) && aux.getIzq().getValor() == nodo.getValor())) {
                arbol.setRaiz(aux.getIzq());
                encontrarNodo(arbol, nodo);
            }
        } else {
            if (!(Objects.nonNull(aux.getDer()) && aux.getDer().getValor() == nodo.getValor())) {
                arbol.setRaiz(aux.getDer());
                encontrarNodo(arbol, nodo);
            }
        }
    }

}
